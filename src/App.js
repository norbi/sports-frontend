import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Container from 'react-bootstrap/Container';

function App() {
  const Sports = React.lazy(() => import('./pages/Sports'));
  const Events = React.lazy(() => import('./pages/Events'));
  const Outcomes = React.lazy(() => import('./pages/Outcomes'));

  return (
    <Router>
      <Container className="p-3">

        <Suspense fallback={'Please wait...'}>
          <Switch>
            <Route path="/" exact>
              <Sports />
            </Route>
            <Route path="/sports" exact>
              <Sports />
            </Route>
            <Route path="/sports/:id" exact>
              <Events />
            </Route>
            <Route path="/sports/:id/events/:event_id" exact>
              <Outcomes />
            </Route>
          </Switch>
        </Suspense>
      </Container>
    </Router>
  );
}

export default App;
