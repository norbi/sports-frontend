import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Col, Row, Table } from 'react-bootstrap';
import LoadingIndicator from '../containers/LoadingIndicator';
import { useParams } from 'react-router-dom';
import OutcomeListItem from '../containers/OutcomeListItem';

const Outcomes = props => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const { id, event_id } = useParams();

  const fetchData = () => {
    setIsLoading(true);
    axios.get(`${process.env.REACT_APP_BACKEND_URL}/api/v1/outcomes`, {params: {sport_id: id, event_id: event_id}})
      .then(function (response) {
        setError(false);
        setData(response.data);
      })
      .catch(function (error) {
        setError(true);
      })
      .then(function () {
        setIsLoading(false);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Row>
      <Col>
        {error && 'Something went wrong.'}
        {isLoading && <LoadingIndicator />}
        {!isLoading && !error && (<Table>
          <thead>
          <tr>
            <th>#</th>
            <th>Description</th>
            <th>Key dimension</th>
            <th>fdp</th>
          </tr>
          </thead>
          <tbody>
            {
              data
                .sort((firstEl, secondEl) => firstEl.po - secondEl.po )
                .map((item) => <OutcomeListItem outcome={item} key={item.id}/>)
            }
          </tbody>
        </Table>)}
      </Col>
    </Row>
  );
};

export default Outcomes;
