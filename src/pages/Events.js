import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Col, Row, Table } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

import LoadingIndicator from '../containers/LoadingIndicator';
import EventListItem from '../containers/EventListItem';


const Events = props => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const { id } = useParams();

  const fetchData = () => {
    setIsLoading(true);
    axios.get(`${process.env.REACT_APP_BACKEND_URL}/api/v1/events`, {params: {sport_id: id}})
      .then(function (response) {
        setError(false);
        setData(response.data);
      })
      .catch(function (error) {
        setError(true);
      })
      .then(function () {
        setIsLoading(false);
      });
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Row>
      <Col>
        {error && 'Something went wrong.'}
        {isLoading && <LoadingIndicator />}
        {!isLoading && !error && (<Table>
          <thead>
          <tr>
            <th>#</th>
            <th>Description</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          {data
            .sort((firstEl, secondEl) => firstEl.pos - secondEl.pos )
            .map((item) => <EventListItem event={item} key={item.id}/>)
          }
          </tbody>
        </Table>)}
      </Col>
    </Row>
  );
};

export default Events;
