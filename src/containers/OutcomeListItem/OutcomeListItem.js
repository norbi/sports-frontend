import React from 'react';
import PropTypes from 'prop-types';

const OutcomeListItem = props => {
  const { outcome } = props;
  console.log(outcome);

  return (
    <tr>
      <td>{outcome.cpid}</td>
      <td>{outcome.d}</td>
      <td>{outcome.keyDimension}</td>
      <td>{outcome.fdp}</td>
    </tr>
  );
};


OutcomeListItem.propTypes = {
  outcome: PropTypes.shape({
    cpid: PropTypes.number,
    d: PropTypes.string,
    fdp: PropTypes.string,
    h: PropTypes.bool,
    key: PropTypes.string,
    keyDimension: PropTypes.string,
    oid: PropTypes.number,
    po: PropTypes.number,
    pr: PropTypes.string,
    prd: PropTypes.number,
    sd: PropTypes.any,
  }).isRequired
};

export default OutcomeListItem;
