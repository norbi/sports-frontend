import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const SportListItem = (props) => {
  const { sport } = props;

  return (
    <tr>
      <td>{sport.id}</td>
      <td>{sport.desc}</td>
      <td>
        <Link to={`/sports/${sport.id}`}>Events</Link>
      </td>
    </tr>
  );
};

SportListItem.propTypes = {
  sport: PropTypes.shape({
    id: PropTypes.number,
    desc: PropTypes.string,
    pos: PropTypes.number,
  }).isRequired
};

export default SportListItem;
