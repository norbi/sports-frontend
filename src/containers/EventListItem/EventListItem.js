import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const EventListItem = props => {
  const { event } = props;

  return (
    <tr>
      <td>{event.id}</td>
      <td>{event.desc}</td>
      <td>
        <Link to={`/sports/${event.sport_id}/events/${event.id}`}>Outcomes</Link>
      </td>
    </tr>
  );
};

EventListItem.propTypes = {
  event: PropTypes.shape({
    desc: PropTypes.string,
    id: PropTypes.number,
    oppADesc: PropTypes.string,
    oppBDesc: PropTypes.string,
    pos: PropTypes.number,
    sport_id: PropTypes.number,
  }).isRequired
};

export default EventListItem;
